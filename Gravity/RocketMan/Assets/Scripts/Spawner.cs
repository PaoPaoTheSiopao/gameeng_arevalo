﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    int randomAngle;
    private bool isOkayToSpawn = true;
    private float cooldown = 5;
    public GameObject asteroid;


    // Use this for initialization
    void Start () {
        //isOkayToSpawn = true;
	}
	
	// Update is called once per frame
	void Update () {
        randomAngle = Random.Range(210, 310);
        //StartCoroutine(ReadySpawn());
        if(isOkayToSpawn==true)
        {
            isOkayToSpawn = false;
            cooldown = 5;
            SpawnAsteroid();
            Debug.Log("making not okay to spawn");  
        }
        CoolDown();
        //Debug.Log(isOkayToSpawn);
        //Debug.Log("cool = "+cooldown);
    }

    public void SpawnAsteroid()
    {
        GameObject aster = Instantiate(asteroid, new Vector3(this.transform.position.x, this.transform.position.y, 0), transform.rotation);
        Asteroids roids = aster.GetComponent<Asteroids>();

        roids.ConstructAsteroid(randomAngle, 5, 50,2);
    }

    void CoolDown()
    {
        if (cooldown <= 0 && !isOkayToSpawn)
        {
            //Debug.Log("making okayto spawn ");
            isOkayToSpawn = true;
        }
        else

            cooldown -= Time.deltaTime;
    }
    IEnumerator ReadySpawn()
    {
        Debug.Log("readying seconds");
        yield return new WaitForSeconds(5);
        Debug.Log("Wait complete");
        SpawnAsteroid();
    }
}
