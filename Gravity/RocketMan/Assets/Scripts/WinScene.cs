﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        Debug.Log("Started to wait");
        yield return new WaitForSeconds(6.5f);
        SceneManager.LoadScene("GameOver");

    }
}
