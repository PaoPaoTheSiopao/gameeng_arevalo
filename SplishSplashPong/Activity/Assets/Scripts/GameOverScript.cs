﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour {

	
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RestartGame()
    {
        Debug.Log("Pressed");
        SceneManager.LoadScene("Gameplay");
        Score.playerScore = 0;
        EnemyScript.enemyCount = 0;
        EnemyScript.isOkayToSpawn = true;
        EnemyScript.stage = 1;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("StartMenu");
        Score.playerScore = 0;
        EnemyScript.enemyCount = 0;
        EnemyScript.isOkayToSpawn = true;
        EnemyScript.stage = 1;
    }
}
