﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {
    public static int indexScene = 2;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("IndexScene = " + indexScene);
	}
    public void GoToNextLevel()
    {
        SceneManager.LoadScene(indexScene);
        UIText.timer = 10;
    }
    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        indexScene = 2;
    }
}
