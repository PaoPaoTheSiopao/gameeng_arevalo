﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {


    Vector2 acceleration;
    Vector2 velocity;
    Vector2 modification;
    int mass = 1;
    public float angle = 90;
    public float thrust = 100; //0.1f;
    public float gravity;
    float smooth = 5;
    public float maxThrust;
    public GameObject blaster;
    public GameObject blasterSpawnpoint;
    //public Rigidbody2D rb;


    // Use this for initialization
    void Start() {
        //rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {

        /*
        if(Input.GetKey(KeyCode.Space))
        {
            rb.AddForce(transform.up*thrust);
            Debug.Log(thrust);
        }
        */
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(blaster,blasterSpawnpoint.transform.position, transform.rotation);
            //GameObject bla = Instantiate(blaster, blasterSpawnpoint.transform.position, transform.rotation);
            //Projectile ter = bla.GetComponent<Projectile>();
            //ter.ConstructBlaster(angle, 20);

        }
        CheckPosition();
        if (Input.GetKey(KeyCode.A))
        {
            angle += 5;
        }
        if (Input.GetKey(KeyCode.D))
        {
            angle += -5;
        }
        if(angle>=360||angle<=-360)
        {
            angle = 0;
        }
        //Quaternion target = Quaternion.Euler(0, 0, angle);
        //transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        
        if(Input.GetKey(KeyCode.W))
        {
            thrust += 0.05f;
        }
        if(thrust>=maxThrust)
        {
            thrust = maxThrust;
        }
        if(!Input.GetKey(KeyCode.W))
        {
            thrust -= 0.01f;
        }
        if(thrust<=0)
        {
            thrust = 0;
        }
        
        UpdateAngleMove();
        AddForce(modification);
        ApplyGravity(); 
        UpdateMotion();
        Quaternion target = Quaternion.Euler(0, 0, angle);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        Debug.Log(thrust);
        
    }

    public void AddForce(Vector2 force)
    {
        acceleration += force / mass;
    }

    public void UpdateMotion()
    {
        velocity += acceleration;
        this.transform.position = new Vector2(this.transform.position.x + (velocity.x * Time.deltaTime), this.transform.position.y + (velocity.y * Time.deltaTime));
        acceleration = new Vector2(0, 0);
    }

    public void ApplyGravity()
    {
        acceleration.y -= gravity;
    }

    public void UpdateAngleMove()
    {
        float xDegToRad = Mathf.Cos(Mathf.Deg2Rad * angle);
        float yDegToRad = Mathf.Sin(Mathf.Deg2Rad * angle);
        float xCoor = thrust * xDegToRad;
        float yCoor = thrust * yDegToRad;
        modification = new Vector2(xCoor, yCoor);
        
    }

    public void CheckPosition()
    {
        if (this.transform.position.y >= 5.5f || this.transform.position.y <= -5.5f) 
        {
            this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y * -1);
        }
        if (this.transform.position.x >= 11.5f || this.transform.position.x <= -11.5f) 
        {
            this.transform.position = new Vector2(this.transform.position.x * -1, this.transform.position.y);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Asteroids")
        {
            //Destroy(gameObject);
            SceneManager.LoadScene("GameOver");
        }
    }
}
