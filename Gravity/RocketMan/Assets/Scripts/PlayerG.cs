﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerG : MonoBehaviour {

    int angle = 0;
    public float thrust = 100;
    public Rigidbody2D rb;
    float smooth = 5;
    public GameObject mario;
    public GameObject meteor;
    float countDown = 1.5f;
    bool isShipOkay = true;
    bool isGoaled = false;
    float goalCountdown = 2;
    public Animator animator;
    public GameObject explosion;


    // Use this for initialization
    void Start()
    {
        FindObjectOfType<AudioManager>().Play("BackGround");
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!isShipOkay)
        {
            Debug.Log("waitforit");
            countDown -= Time.deltaTime;
        }
        
        if(countDown<=0)
        {
            Debug.Log("Next Scene");
            Destroy(gameObject);
            HelpText.playerDeath += 1;
            SceneManager.LoadScene("Crash");

        }
        if(isGoaled)
        {
            goalCountdown -= Time.deltaTime;
        }
        if(goalCountdown<=0)
        {
            Debug.Log("won");
            SceneManager.LoadScene("WinScene");
        }
        if(Input.GetKey(KeyCode.Space))
        {
            if (isShipOkay)
            {
                FindObjectOfType<AudioManager>().Play("Thrust");
                rb.AddForce(transform.up * thrust);
                animator.SetBool("isThrusting", true);
            }
            //Debug.Log(thrust);
        }
        if (!Input.GetKey(KeyCode.Space))
        {
            
            animator.SetBool("isThrusting", false);
            //Debug.Log(thrust);
        }

        CheckPosition();
        if (Input.GetKey(KeyCode.A))
        {
            angle += 3;
        }
        if (Input.GetKey(KeyCode.D))
        {
            angle += -3;
        }
        if (angle >= 360 || angle <= -360)
        {
            angle = 0;
        }
        Quaternion target = Quaternion.Euler(0, 0, angle);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        //Debug.Log(angle);
        Debug.Log(thrust);
    }

   

    public void CheckPosition()
    {
        if (this.transform.position.y >= 7.5f || this.transform.position.y <= -7.5f)
        {
            SceneManager.LoadScene("LostInSpace");
        }
        if (this.transform.position.x >= 12.5f || this.transform.position.x <= -12.5f)
        {
            SceneManager.LoadScene("LostInSpace");
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Mountain")
        {
            Debug.Log("collided with mountain");
            Instantiate(explosion, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 5), transform.rotation);
            if (isShipOkay)
            {

                FindObjectOfType<AudioManager>().Play("ExplodeShip");
            }
            isShipOkay = false;
        }
        if (collision.gameObject.tag == "Mario")
        {
            Instantiate(explosion, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 5), transform.rotation);
            Debug.Log("collided with mario");
            if (isShipOkay)
            {
                FindObjectOfType<AudioManager>().Play("ExplodeShip");
            }
            isShipOkay = false;
        }
        if (collision.gameObject.tag == "Goal")
        {
            if (angle >= 66 || angle <= -66)
            {
                Instantiate(explosion, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 5), transform.rotation);
                if (isShipOkay)
                {
                    FindObjectOfType<AudioManager>().Play("ExplodeShip");
                }
                isShipOkay = false;
                Debug.Log("collided with Goal wrong angle");
            }
            if(collision.gameObject.name=="Goal1")
            {
                if (angle <= 66 || angle >= -66)
                {
                    FindObjectOfType<AudioManager>().Play("SpawnMario");
                    Instantiate(mario, new Vector3(this.transform.position.x, this.transform.position.y + 8, 0), transform.rotation);
                }
                isGoaled = true;
                goalCountdown = 5;
            }
            if (collision.gameObject.name == "Goal2")
            {
                Instantiate(meteor, new Vector3(this.transform.position.x, this.transform.position.y + 8, 0), transform.rotation);
                isGoaled = true;
                goalCountdown = 5;
            }
            if (collision.gameObject.name == "Goal3" || collision.gameObject.name == "Goal4" || collision.gameObject.name == "Goal5")
            {
                isGoaled = true;
                goalCountdown = 2;
            }
            Debug.Log("collided with Goal");
        }
    }
}
