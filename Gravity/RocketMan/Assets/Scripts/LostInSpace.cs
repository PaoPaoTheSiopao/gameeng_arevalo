﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LostInSpace : MonoBehaviour {

	// Use this for initialization
	void Start () {
        FindObjectOfType<AudioManager>().Play("Wah");
    }
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(Ready());
	}
    IEnumerator Ready()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("GameOver");

    }
}
