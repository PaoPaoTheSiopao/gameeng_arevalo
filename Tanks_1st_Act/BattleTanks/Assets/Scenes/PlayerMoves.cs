﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoves : MonoBehaviour {


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MoveObject(Vector3 locations)
    {
        
        if (Vector3.Distance(this.transform.position, locations) != 0.0f)
        {
            
            this.transform.position = Vector3.MoveTowards(this.transform.position, locations, 10 * Time.deltaTime);
        }
    }
}
