﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Move : MonoBehaviour {

    public float speed = 5;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        MovePlayer();
        if(Input.GetMouseButtonDown(0))
        {
            speed *= -1;
        }
    }

    void MovePlayer()
    {
        this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + (speed * Time.deltaTime));
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("collide3d");
        if (collision.gameObject.tag=="Milk")
        {
            speed *= -1;

        }
        
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("collide2d");
        if (collision.gameObject.tag=="Wall")
        {
            Debug.Log("change direction");
            speed *= -1;
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("collideMonster");
            Destroy(gameObject);
            SceneManager.LoadScene("GameOver");
        }
    }
}
