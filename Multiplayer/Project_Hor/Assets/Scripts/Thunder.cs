﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thunder : MonoBehaviour {

    float thunderCD = 8;
    float thunderWaveOne = 0.2f;
    float thunderWaveTwo = 5.5f;
    bool isThunderInEffect = false;
    bool playedThunderSND = false;
    bool isOkayToStrikeOne = false;
    bool isOkayToStrikeTwo = false;

    Light thunder;

	// Use this for initialization
	void Start () {
        thunder = GetComponent<Light>();
        
    }
	
	// Update is called once per frame
	void Update () {
        thunderCD -= Time.deltaTime;
        if(thunderCD<=0)
        {
            isThunderInEffect = true;
        }
        if(isThunderInEffect)
        {
            if (!playedThunderSND)
            {
                FindObjectOfType<AudioManager>().Play("Thunder");
                playedThunderSND = true;
                isOkayToStrikeOne = true;
                isOkayToStrikeTwo = true;
            }
            thunderWaveOne -= Time.deltaTime;
            thunderWaveTwo -= Time.deltaTime;
            if (thunderWaveOne<=0)
            {
                if (isOkayToStrikeOne)
                {
                    thunder.intensity = 14;
                    isOkayToStrikeOne = false;
                }
            }
            if (thunderWaveTwo <= 0)
            {
                if (isOkayToStrikeTwo)
                {
                    thunder.intensity = 25;
                    isOkayToStrikeTwo = false;
                }
            }
            if (thunderCD<=0&&thunderWaveOne<=0&&thunderWaveTwo<=0)
            {
                isThunderInEffect = false;
                thunderWaveOne = 0.2f;
                thunderWaveTwo = 5.5f;
                thunderCD = 8;
                playedThunderSND = false;
            }
        }
        thunder.intensity -= 10 * Time.deltaTime;
        Debug.Log("ThunderCD = " + thunderCD + " ThunderWaveOne = " + thunderWaveOne + " ThunderWaveTwo = " + thunderWaveTwo + " isThuderInEffect = " + isThunderInEffect);
        
	}
}
