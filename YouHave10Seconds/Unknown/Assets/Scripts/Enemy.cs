﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour {

    public float maxAngle;
    public float maxRadius;
    public static bool isInFOV = false;
    public float rotateSpeed;


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, maxRadius);

        //Vector3 fovLine1 = Quaternion.AngleAxis(maxAngle, transform.forward) * transform.right * maxRadius;
        //Vector3 fovLine2 = Quaternion.AngleAxis(-maxAngle, transform.forward) * transform.right * maxRadius;

        var angle1 = Quaternion.AngleAxis(maxAngle, transform.forward) * transform.right * maxRadius;
        var angle2 = Quaternion.AngleAxis(-maxAngle, transform.forward) * transform.right * maxRadius;

        

        Gizmos.color = Color.blue;
        //Gizmos.DrawRay(transform.position, angle1);
        //Gizmos.DrawRay(transform.position, angle2);

        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z), angle1);
        Gizmos.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z), angle2);

        if (!isInFOV)
        {
            Gizmos.color = Color.red;
        }
        else
            Gizmos.color = Color.yellow;
        

        Gizmos.color = Color.black;
        Gizmos.DrawRay(transform.position, transform.right * maxRadius);
    }

    public static bool InFov(Transform theEnemy, float maxAngle, float maxRadius)
    {
        var angle1 = Quaternion.AngleAxis(maxAngle, theEnemy.forward) * theEnemy.right * maxRadius;
        var angle2 = Quaternion.AngleAxis(-maxAngle, theEnemy.forward) * theEnemy.right * maxRadius;
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(theEnemy.transform.position.x, theEnemy.transform.position.y), angle1, maxRadius);
        RaycastHit2D hit2 = Physics2D.Raycast(new Vector2(theEnemy.transform.position.x, theEnemy.transform.position.y), angle2, maxRadius );
        RaycastHit2D hit3 = Physics2D.Raycast(new Vector2(theEnemy.transform.position.x, theEnemy.transform.position.y), theEnemy.right, maxRadius );
        //RaycastHit2D hit2 = Physics2D.Raycast(theEnemy.position, angle2, maxRadius);
        //RaycastHit2D hit3 = Physics2D.Raycast(theEnemy.position, theEnemy.right, maxRadius);

        if (hit.collider != null) 
        {
            //Debug.Log(hit.collider);
            if (hit.collider.gameObject.tag == "Player")
            {
                Debug.Log("hit positive");
                return true;
            }
            
            else
                //Debug.Log("Nothing1");
            return false;
        }
        else if(hit2.collider != null)
        {
            //Debug.Log("WTF2");
            if (hit2.collider.gameObject.tag == "Player")
            {
                Debug.Log("hit negative");
                return true;
            }
            else
                //Debug.Log("Nothing2");
            return false;
        }
        else if (hit3.collider != null)
        {
            //Debug.Log("WTF3");
            if (hit3.collider.gameObject.tag == "Player")
            {
                Debug.Log("hit middle");
                return true;
            }
            else
               // Debug.Log("Nothing3");
                return false;
        }
        else
        {
            return false;
        }

    }

    private void Update()
    {
        Debug.Log("FOV =" + isInFOV);
        isInFOV = InFov(transform, maxAngle, maxRadius);
        transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);
        if(isInFOV)
        {
            FindObjectOfType<AudioManager>().Play("AlertedGuard");
            StartCoroutine(Wait());
            
            
            GameManager.detected = true;
        }
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collided with shuriken");
        if(collision.gameObject.tag=="Projectile")
        {
            Debug.Log("destroyed by shuriken");
            FindObjectOfType<AudioManager>().Play("ShurikenImpact");
            Destroy(gameObject);
            Score.enemyKilled += 1;
        }
    }

    IEnumerator Wait()
    {
        print(Time.time);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("GameOver");
        print(Time.time);
    }
    
}
