﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    GameObject spawnPoint;
    public GameObject player;
    public static bool detected = false;

	// Use this for initialization
	void Start () {
        spawnPoint = GameObject.FindGameObjectWithTag("SpawnPoint");
        Instantiate(player, spawnPoint.transform.position, transform.rotation);
        if(detected)
        {
            Debug.Log("You're DEAD!!!!!!");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
