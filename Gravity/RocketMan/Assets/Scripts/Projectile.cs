﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    Vector2 acceleration;
    Vector2 velocity;
    Vector2 modification;
    int mass = 1;
    public float speed = 20;
    public float angle;

    private void Awake()
    {
        GameObject pla = GameObject.FindGameObjectWithTag("SpaceShip");
        Player yer = pla.GetComponent<Player>();
        angle = yer.angle;
        float xDegToRad = Mathf.Cos(Mathf.Deg2Rad * angle);
        float yDegToRad = Mathf.Sin(Mathf.Deg2Rad * angle);
        float xCoor = speed * xDegToRad;
        float yCoor = speed * yDegToRad;
        modification = new Vector2(xCoor, yCoor);

    }
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckPosition();
        //UpdateAngleMove();
        AddForce(modification);
        UpdateMotion();
    }

    public void AddForce(Vector2 force)
    {
        acceleration += force / mass;
    }

    public void ConstructBlaster (float mAngle, float spd)
    {
        angle = mAngle;
        speed = spd;
    }

    public void UpdateAngleMove()
    {
        
        //Debug.Log("Modi = " + modification);
    }

    public void UpdateMotion()
    {
        velocity += acceleration;
        this.transform.position = new Vector2(this.transform.position.x + (velocity.x * Time.deltaTime), this.transform.position.y + (velocity.y * Time.deltaTime));
        Debug.Log("Blaster Accel = " + acceleration);
        acceleration = new Vector2(0, 0);
    }

    public void CheckPosition()
    {
        if (this.transform.position.y >= 6.5f || this.transform.position.y <= -6.5f || this.transform.position.x >= 12.5f || this.transform.position.x <= -12.5f) 
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Asteroids")
        {
            Destroy(gameObject);
        }
    }
}
