﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroids : MonoBehaviour {

    public Vector2 acceleration;
    public Vector2 velocity;
    public Vector2 modification;
    int mass = 1;
    public float speed;
    public int rotateSpeed;
    public float asteroidSize;
    public int angle;
    public Asteroids aster;
    public CircleCollider2D collisionBoy;


    // Use this for initialization
    void Start()
    {
        aster = GetComponent<Asteroids>();
        aster.enabled = true;
        collisionBoy = GetComponent<CircleCollider2D>();
        collisionBoy.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        CheckPosition();
        transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);
        UpdateAngleMove();
        AddForce(modification);
        UpdateMotion();
        

    }

    public void AddForce(Vector2 force)
    {
        acceleration += force / mass;
    }

    public void ConstructAsteroid(int mAngle,float spd,int rotSpeed,float size)
    {
        angle = mAngle;
        speed = spd;
        rotateSpeed = rotSpeed;
        asteroidSize = size;
        this.transform.localScale = new Vector3(size, size, 0);
    }

    public void UpdateAngleMove()
    {
        float xDegToRad = Mathf.Cos(Mathf.Deg2Rad * angle);
        float yDegToRad = Mathf.Sin(Mathf.Deg2Rad * angle);
        float xCoor = speed * xDegToRad;
        float yCoor = speed * yDegToRad;
        modification = new Vector2(xCoor, yCoor);
        //Debug.Log("Modi = " + modification);
    }

    public void UpdateMotion()
    {
        velocity = acceleration;
        this.transform.position = new Vector2(this.transform.position.x + (velocity.x * Time.deltaTime), this.transform.position.y + (velocity.y * Time.deltaTime));
        //Debug.Log("Accel = " + acceleration);
        acceleration = new Vector2(0, 0);
    }

    public void CheckPosition()
    {
        if (this.transform.position.y >= 6.5f || this.transform.position.y <= -6.5f)
        {
            Destroy(gameObject);
        }
        if (this.transform.position.x >= 12.5f || this.transform.position.x <= -12.5f)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="Mountain")
        {
            FindObjectOfType<AudioManager>().Play("AsteroidSmash");
            Destroy(gameObject);
            
        }
    }

}
