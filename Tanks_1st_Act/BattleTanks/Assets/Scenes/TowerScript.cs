﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour {

    public GameObject spawnPoint;
    public GameObject bullet;
    private float count = 0;
    public float spawnTime;
    public float spawnDelay;
    public bool stopSpawning = false;

	// Use this for initialization
	void Start () {
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
	}   
	
	// Update is called once per frame
	public void SpawnObject()
    {
        Instantiate(bullet, spawnPoint.transform.position, transform.rotation);
        if(stopSpawning)
        {
            CancelInvoke("SpawnObject");
        }
    }
}
