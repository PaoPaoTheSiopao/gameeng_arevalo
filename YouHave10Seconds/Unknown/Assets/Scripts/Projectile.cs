﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    Vector3 targetPosition = Vector3.zero;
    public float speed;

	// Use this for initialization
	void Start () {
        targetPosition = MousePosition.mouseClickPosition;
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("Bullet moving" + " position ="+targetPosition);
        this.transform.position = Vector3.MoveTowards(this.transform.position, targetPosition, Time.deltaTime * speed);
        if(transform.position==targetPosition)
        {
            Destroy(gameObject);
        }
        transform.Rotate(0, 0, 1000*Time.deltaTime);
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="Wall")
        {
            Destroy(gameObject);
        }
    }

}
