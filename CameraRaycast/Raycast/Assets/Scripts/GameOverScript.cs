﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {


    // Update is called once per frame
    void Update()
    {

    }

    public void RestartGame()
    {
        Debug.Log("Pressed");
        SceneManager.LoadScene("Gameplay");
        Score.playerScore = 0;
        ComplaintsScript.peopleComplaints = 0;

    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Score.playerScore = 0;
        ComplaintsScript.peopleComplaints = 0;
        
    }
}

