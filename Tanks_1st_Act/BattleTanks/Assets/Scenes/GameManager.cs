﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
   
    public List<GameObject> locations;
    public GameObject player;
    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKey(KeyCode.Alpha1))
        {
            MoveObject(0);
        }

        if (Input.GetKey(KeyCode.Alpha2))
        {
            MoveObject(1);
        }

        if (Input.GetKey(KeyCode.Alpha3))
        {
            MoveObject(2);
        }

        if (Input.GetKey(KeyCode.Alpha4))
        {
            MoveObject(3);
        }

        if (Input.GetKey(KeyCode.Alpha5))
        {
            MoveObject(4);
        }

        if (Input.GetKey(KeyCode.Alpha6))
        {
            MoveObject(5);
        }

        if (Input.GetKey(KeyCode.Alpha7))
        {
            MoveObject(6);
        }
        //
        if (Input.GetKey(KeyCode.Q))
        {
            MoveObject(7);
        }

        if (Input.GetKey(KeyCode.W))
        {
            MoveObject(8);
        }

        if (Input.GetKey(KeyCode.E))
        {
            MoveObject(9);
        }

        if (Input.GetKey(KeyCode.R))
        {
            MoveObject(10);
        }

        if (Input.GetKey(KeyCode.T))
        {
            MoveObject(11);
        }

        if (Input.GetKey(KeyCode.Y))
        {
            MoveObject(12);
        }

        if (Input.GetKey(KeyCode.U))
        {
            MoveObject(13);
        }
        //
        if (Input.GetKey(KeyCode.A))
        {
            MoveObject(14);
        }

        if (Input.GetKey(KeyCode.S))
        {
            MoveObject(15);
        }

        if (Input.GetKey(KeyCode.D))
        {
            MoveObject(16);
        }

        if (Input.GetKey(KeyCode.F))
        {
            MoveObject(17);
        }

        if (Input.GetKey(KeyCode.G))
        {
            MoveObject(18);
        }

        if (Input.GetKey(KeyCode.H))
        {
            MoveObject(19);
        }

        if (Input.GetKey(KeyCode.J))
        {
            MoveObject(20);
        }
        //
        if (Input.GetKey(KeyCode.Z))
        {
            MoveObject(21);
        }

        if (Input.GetKey(KeyCode.X))
        {
            MoveObject(22);
        }

        if (Input.GetKey(KeyCode.C))
        {
            MoveObject(23);
        }

        if (Input.GetKey(KeyCode.V))
        {
            MoveObject(24);
        }

        if (Input.GetKey(KeyCode.B))
        {
            MoveObject(25);
        }

        if (Input.GetKey(KeyCode.N))
        {
            MoveObject(26);
        }

        if (Input.GetKey(KeyCode.M))
        {
            MoveObject(27);
        }
    }
    void MoveObject(int index)
    {
        Debug.Log("moving");
        if (Vector3.Distance(player.transform.position,locations[index].transform.position ) != 0.0f)
        {
            Debug.Log("moves");
            player.transform.position = Vector3.MoveTowards(player.transform.position, locations[index].transform.position, 5 * Time.deltaTime);
        }
    }
}
