﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawnpoint : MonoBehaviour {

    public GameObject[] people;
    int randomSpawnPeople;
    float randomSpawnPoint;
    public static float coolDownManager;
    public float spawnTime;
    public float spawnDelay;
    public float bombSpawnTime;
    public float bombSpawnDelay;
    public float coolDownForPeople;
    public float coolDownForBombPeople;
    private bool isOkayToSpawnPeople = false;
    private bool isOkayToSpawnBombPeople = false;

    // Use this for initialization
    void Start () {
        //InvokeRepeating("SpawnPeople", spawnTime, spawnDelay);
        //InvokeRepeating("SpawnBombPeople", bombSpawnTime, bombSpawnDelay);
        FindObjectOfType<AudioManager>().Play("AirportAmbiance");
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("cooldown = " + coolDownForPeople);
        //Debug.Log("cooldownBomb = " + coolDownForBombPeople);
        //StartCoroutine(ReadySpawn());
        CoolDown();
        if(isOkayToSpawnPeople)
        {
            SpawnPeople();
            //SpawnBombPeople();
            ManageCooldown();
            isOkayToSpawnPeople = false;
            
        }
        if(isOkayToSpawnBombPeople)
        {
            SpawnBombPeople();
            ManageCooldown();
            isOkayToSpawnBombPeople = false;
            
        }
        

    }

    void SpawnPeople()
    {
        randomSpawnPeople = Random.Range(0, 6);
        randomSpawnPoint = Random.Range(-4.7f, 2.5f);
        Debug.Log("called spawnpeople");
        GameObject ene = Instantiate(people[randomSpawnPeople], new Vector3(this.transform.position.x, randomSpawnPoint, this.transform.position.z), transform.rotation);
        PeopleScript enem = ene.GetComponent<PeopleScript>();
        enem.setPeopleValue(false, 2);
    }
    
    void SpawnBombPeople()
    {
        randomSpawnPeople = Random.Range(0, 6);
        randomSpawnPoint = Random.Range(-4.7f, 2.5f);
        Debug.Log("called spawnBOMBpeople");
        GameObject ene = Instantiate(people[randomSpawnPeople], new Vector3(this.transform.position.x, randomSpawnPoint, this.transform.position.z), transform.rotation);
        PeopleScript enem = ene.GetComponent<PeopleScript>();
        enem.setPeopleValue(true, 4);
    }

    void CoolDown()
    {
        if (coolDownForPeople <= 0)
        {
            isOkayToSpawnPeople = true;
        }
        if (coolDownForBombPeople <= 0)
        {
            isOkayToSpawnBombPeople = true;
        }
        else
        
            coolDownForPeople -= Time.deltaTime;

        coolDownForBombPeople -= Time.deltaTime;

    }

    void ManageCooldown()
    {
        Debug.Log("Called ManageCooldown");
        if (Score.playerScore >= 500)
        {
            if (isOkayToSpawnPeople)
            {
                coolDownForPeople = 0.1f;
            }
            else if (isOkayToSpawnBombPeople)
            {
                coolDownForBombPeople = 0.7f;
            }
        }
        else if (Score.playerScore >= 400)
        {
            if (isOkayToSpawnPeople)
            {
                coolDownForPeople = 0.5f;
            }
            else if (isOkayToSpawnBombPeople)
            {
                coolDownForBombPeople = 1;
            }
        }
        else if (Score.playerScore >= 300)
        {
            if (isOkayToSpawnPeople)
            {
                coolDownForPeople = 0.7f;
            }
            else if (isOkayToSpawnBombPeople)
            {
                coolDownForBombPeople = 1.5f;
            }
        }
        else if (Score.playerScore >= 200)
        {
            if (isOkayToSpawnPeople)
            {
                coolDownForPeople = 1;
            }
            else if (isOkayToSpawnBombPeople)
            {
                coolDownForBombPeople = 2;
            }
        }
        else if (Score.playerScore >= 100)
        {
            if (isOkayToSpawnPeople)
            {
                coolDownForPeople = 2;
            }
            else if(isOkayToSpawnBombPeople) 
            {
                Debug.Log("BombManage");
                coolDownForBombPeople = 3;
            }
        }
        else
            if(isOkayToSpawnPeople)
        {
            coolDownForPeople = 3;
        }
        else if (isOkayToSpawnBombPeople)
        {
            coolDownForBombPeople = 4;
        }
    }

    IEnumerator ReadySpawn()
    {
        Debug.Log("readying seconds");
        yield return new WaitForSeconds(2);
        Debug.Log("Wait complete");
        randomSpawnPeople = Random.Range(0, 3);
        randomSpawnPoint = Random.Range(-4.7f, 2.5f);
        //SpawnPeople(randomSpawnPeople, randomSpawnPoint);
    }
}
