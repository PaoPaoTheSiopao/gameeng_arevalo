﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScorePointer : MonoBehaviour {
    public GameObject upSpawn;
    public GameObject downSpawn;
    public GameObject pointSprite;
    bool upIsAvail = true;
    bool downIsAvail = false;
    public static bool isSpawnOkay = true;
	// Use this for initialization
	void Start () {
        //InvokeRepeating("SpawnDown", spawnRate,spawnRate);
        //InvokeRepeating("SpawnUp", spawnRate/2, spawnRate);
        isSpawnOkay = true;
    }

    // Update is called once per frame
    void Update()
    {
       if(isSpawnOkay)
        {
            Spawn();
        }
    }

    public void Spawn()
    {
        isSpawnOkay = false;
        Debug.Log(isSpawnOkay);
        if(upIsAvail)
        {
            Instantiate(pointSprite, upSpawn.transform.position, transform.rotation);
            upIsAvail = false;
            downIsAvail = true;
        }
        else if(downIsAvail)
        {
            Instantiate(pointSprite, downSpawn.transform.position, transform.rotation);
            downIsAvail = false;
            upIsAvail = true;
        }
    }

}
