﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

    GameObject target;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullets")
        {
            DoDamage(target);
            Destroy(other.gameObject);
        }
    }

    public void DoDamage(GameObject targ)
    {
        Health h = targ.GetComponent<Health>();
        h.TakeDamage(10);
    }
}
