﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    Text scoreText;
    public static int playerScore;
    void Start()
    {
        scoreText = GetComponent<Text>();
    }
    // Update is called once per frame
    void Update () {
        scoreText.text = "Score: " + playerScore;
        if (playerScore >= 1200)
        {
            Spawnpoint.coolDownManager = 0.1f;
        }
        else if (playerScore  >= 900)
        {
            Spawnpoint.coolDownManager = 0.5f;
        }
        else if (playerScore >= 600)
        {
            Spawnpoint.coolDownManager = 1;
        }
        else if (playerScore >= 300)
        {
            Spawnpoint.coolDownManager = 2;
        }
        else if (playerScore >= 100)
        {
            Spawnpoint.coolDownManager = 3;
        }
    }
}
