﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIText : MonoBehaviour {


    public static float timer = 10;
    Text time;

	// Use this for initialization
	void Start () {
        time = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        time.text = "Time : " + timer;
        timer -= 1 * Time.deltaTime;
        if (timer <= 0)
        {
            
            SceneManager.LoadScene("GameOver");
        }
        if(timer<=5)
        {
            Debug.Log("calling loosing time");
            FindObjectOfType<AudioManager>().Play("LoosingTime");
        }

	}
}
