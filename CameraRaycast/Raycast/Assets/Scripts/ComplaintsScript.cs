﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ComplaintsScript : MonoBehaviour {

    Text complains;
    public static int peopleComplaints;
    void Start()
    {
        complains = GetComponent<Text>();
    }
    // Update is called once per frame
    void Update()
    {
        complains.text = "Complaints: " + (10- peopleComplaints);
        if(peopleComplaints>=10)
        {
            SceneManager.LoadScene("GameOverSceneTwo");
        }
    }
}
