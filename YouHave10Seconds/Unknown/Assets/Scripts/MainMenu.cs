﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartTheGame()
    {
        SceneManager.LoadScene("Level1");
        UIText.timer = 10;
    }

    public void QuitTheGame()
    {
        Application.Quit();
    }
}
