﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public static int playerScore = 0;
    Text score;

	// Use this for initialization
	void Start () {
        score = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        score.text = "Score : " + playerScore;
        if(playerScore==10)
        {
            EnemyScript.stage = 2;
        }
        if(playerScore == 30)
        {
            EnemyScript.stage = 3;
        }
	}
    
    public void ResetScore()
    {
        Debug.Log("resetted score");
        playerScore = 0;
    }

}
