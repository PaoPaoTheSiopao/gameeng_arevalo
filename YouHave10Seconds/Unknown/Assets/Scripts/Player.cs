﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    public float speed;
    public GameObject shuriken;
    public GameObject spawnpoint;
    public int shurikenAmount;
    private bool detected = false;
    public Animator animator;
    float x;
    float y;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.position += new Vector3(x, y, 0) * (speed * Time.deltaTime);
        if (Input.anyKey)
        {
            animator.SetFloat("Speed", 1);
        }
        else
        {
            animator.SetFloat("Speed", 0);
        }
        //animator.SetFloat("Speed", Mathf.Abs(y));

        if (Input.GetMouseButtonUp(0))
        {
            if (shurikenAmount > 0)
            {
                Instantiate(shuriken, spawnpoint.transform.position, transform.rotation);
                shurikenAmount--;
            }
            else
                return;
        }
        
        UpdateStatus();
        if(detected)
        {
            Debug.Log("You're DEAD!!!!!!!!!");
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Wall")
        {
            Debug.Log("collided");
        }
        if(collision.gameObject.tag=="Objective")
        {
            SceneManager.LoadScene("NextLevel");
            Score.playerScore += 5000;
            NextLevel.indexScene += 1;
        }
    }

    void UpdateStatus()
    {
        Debug.Log("Updating Status");
        Debug.Log(detected);
    }
 
}
