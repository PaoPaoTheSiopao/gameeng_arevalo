﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {
    public int speed = 4;
    public static int testc = 5;
    public int position = 0;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        MovePlayer();
        if (this.transform.position.x > 3.5)
        {
            Destroy(gameObject);
            EnemyScript.enemyCount--;
        }
        if (this.transform.position.x < -3.5)
        {
            Destroy(gameObject);
            EnemyScript.enemyCount--;
        }
    }

    void MovePlayer()
    {

        this.transform.position = new Vector2(this.transform.position.x + (speed * Time.deltaTime), this.transform.position.y);
   
    }
    public void SetValues(int sped,float sca)
    {
        this.speed = sped;
        this.transform.localScale = new Vector3(sca, sca, 1);
    }
}
