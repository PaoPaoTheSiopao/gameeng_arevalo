﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGameManager : MonoBehaviour {

    public List<GameObject> locations;
    public GameObject enemy;
    
	
	// Update is called once per frame
	void Update () {
		
	}

    void MoveEnemy(int index)
    {
        Debug.Log("moving");
        if (Vector3.Distance(enemy.transform.position, locations[index].transform.position) != 0.0f)
        {
            Debug.Log("moves");
            enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, locations[index].transform.position, 10 * Time.deltaTime);
        }
    }
}
