﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    public GameObject playerPos;
    private Vector3 target;

	// Use this for initialization
	void Start () {
        playerPos = GameObject.FindGameObjectWithTag("Player");
        target = playerPos.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = Vector3.MoveTowards(this.transform.position, target, 20 * Time.deltaTime);
        if(this.transform.position == target)
        {
            Destroy(gameObject);
        }
        if(this.transform.position == playerPos.transform.position)
        {
            Attack a = playerPos.GetComponent<Attack>();
            a.DoDamage(playerPos);
            Destroy(gameObject);
        }
	}
}
