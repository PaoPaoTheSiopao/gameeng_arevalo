﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public GameObject enemy;
    public GameObject spawnRight;
    public GameObject spawnLeft;
    public static int enemyCount;
    GameObject enem;
    public static bool isOkayToSpawn = true;
    public static int stage = 1;

	// Use this for initialization
	void Start () {
        isOkayToSpawn = true;
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(stage);
        if(isOkayToSpawn)
        {
            if(stage*2 >= enemyCount)
            {
                Debug.Log("spawned");
                SpawnEnemy();
            }
        }
        if (enemyCount >= stage * 3)
        {
            isOkayToSpawn = false;
        }
        if(enemyCount<=stage*3)
        {
            isOkayToSpawn = true;
        }

	}
    void SpawnEnemy()
    {
        if(Random.Range(1,100)>50)
        {

            if (Random.Range(1, 100) > 50)
            {
                GameObject ene = Instantiate(enemy, new Vector3(spawnLeft.transform.position.x, Random.Range(-2, 2), spawnLeft.transform.position.z), transform.rotation);
                EnemyMove emen = ene.GetComponent<EnemyMove>();
                emen.SetValues(2,0.13f);
                enemyCount++;
                Debug.Log("Left Big " + ene.transform.position.x + "EnemyCount = " + enemyCount);
            }
            if (Random.Range(1, 100) < 50)
            {
                GameObject ene = Instantiate(enemy, new Vector3(spawnLeft.transform.position.x, Random.Range(-2, 2), spawnLeft.transform.position.z), transform.rotation);
                EnemyMove emen = ene.GetComponent<EnemyMove>();
                enemyCount++;
                Debug.Log("Left small " + ene.transform.position.x + "EnemyCount = " + enemyCount);
                emen.SetValues(4, 0.09f);
            }
        }
        if (Random.Range(1, 100) < 50)
        {
            if (Random.Range(1, 100) > 50)
            {
                GameObject ene = Instantiate(enemy, new Vector3(spawnRight.transform.position.x, Random.Range(-2, 2), spawnLeft.transform.position.z), transform.rotation);
                EnemyMove emen = ene.GetComponent<EnemyMove>();
                enemyCount++;
                Debug.Log("Right Big " + ene.transform.position.x + "EnemyCount = " + enemyCount);
                emen.SetValues(-2, 0.13f);
            }
            if (Random.Range(1, 100) < 50)
            {
                GameObject ene = Instantiate(enemy, new Vector3(spawnRight.transform.position.x, Random.Range(-2, 2), spawnLeft.transform.position.z), transform.rotation);
                EnemyMove emen = ene.GetComponent<EnemyMove>();
                enemyCount++;
                Debug.Log("right small " + ene.transform.position.x + "EnemyCount = "+enemyCount);
                emen.SetValues(-4, 0.09f);
            }
        }
    }
}
