﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverSceneOneAudio : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InvokeRepeating("PlaySound", 1, 10);
    }
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(Wait());
	}

    void PlaySound()
    {
        FindObjectOfType<AudioManager>().Play("Plane");
    }
    IEnumerator Wait()
    {
        Debug.Log("Started to wait");
        yield return new WaitForSeconds(6.5f);
        SceneManager.LoadScene("GameOver");
        
    }
}
