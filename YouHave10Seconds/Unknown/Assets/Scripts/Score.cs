﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public static int playerScore;
    private int totalScore;
    public static int enemyKilled;
    Text score;

	// Use this for initialization
	void Start () {
        score = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        
        totalScore = playerScore - (enemyKilled * 500);
        score.text = "Score = " + totalScore;
    }
}
