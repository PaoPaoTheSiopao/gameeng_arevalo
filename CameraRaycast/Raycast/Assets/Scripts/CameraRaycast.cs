﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycast : MonoBehaviour {

    //Camera cam;
    float x, y;
    public int speed;

    void Start()
    {
        //cam = GetComponent<Camera>();
    }

    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.position += new Vector3(x, y, 0) * (speed * Time.deltaTime);
        if (Input.GetMouseButtonUp(0))
        {
            CastRay();
        }
    }

    void CastRay()
    {
        Vector2 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(target, Vector2.zero);
        
        if(hit.collider!=null)
        {
            Debug.Log("Hit Target = " + hit.collider.gameObject.name);
            GameObject peo = hit.collider.gameObject;
            PeopleScript peop = peo.GetComponent<PeopleScript>();
            FindObjectOfType<AudioManager>().Play("PoliceYelling");
            if(peop.suicideBomber)
            {
                Debug.Log("SuicideBOmber Captured");
                Score.playerScore += 100;
            }
            else
            {
                Debug.Log("False Accusation");
                ComplaintsScript.peopleComplaints += 1;
            }
            Destroy(hit.collider.gameObject);
        }
        else
        {
            Debug.Log("You hit nothing");
        }
    }

}
