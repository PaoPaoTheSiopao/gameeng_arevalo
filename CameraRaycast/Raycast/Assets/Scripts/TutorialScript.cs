﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        FindObjectOfType<AudioManager>().Play("LearnSong");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void StartTheGame()
    {
        SceneManager.LoadScene("GamePlay");
    }
}
