﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PeopleScript : MonoBehaviour {

    public float speed;
    public bool suicideBomber = false;
    public Animator animator;
    public GameObject particle;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        MovePeople();
        animator.SetBool("isWithBomb", suicideBomber);
	}

    void MovePeople()
    {
        this.transform.position = new Vector2(this.transform.position.x + (speed * Time.deltaTime), this.transform.position.y);
    }

    public void setPeopleValue(bool bomb,int moveSpeed)
    {
        suicideBomber = bomb;
        speed = moveSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Departure")
        {
           //Debug.Log("Departed");
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Departure")
        {
            Instantiate(particle, new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z-5), transform.rotation);
            Debug.Log("Departed");
            if(suicideBomber)
            {
                
                SceneManager.LoadScene("GameOverSceneOne");
            }
            Destroy(gameObject);
        }
    }
}
