﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePointScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("scored");
            Score.playerScore += 1;
            Destroy(this);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            Debug.Log("scored");
            Score.playerScore += 1;
            Destroy(gameObject);
            SpawnScorePointer.isSpawnOkay = true;
            Debug.Log(SpawnScorePointer.isSpawnOkay);
        }
    }
}
